import http.client
import json

def decode_url(url):
    u = url.split('/')
    if 'https://' in url:
        base = u[2]
        path = '/' + '/'.join(u[3:])
    else:
        base = u[0]
        path = '/' + '/'.join(u[1:])

    return base, path


def http_post(url, headers={}, body={}):
    h = {'Content-type': 'application/json'}
    h.update(headers)
    base, path = decode_url(url)
    conn = http.client.HTTPSConnection(base)
    conn.request("POST", url=path, body=body, headers=h)
    response = conn.getresponse()
    status = response.status
    reason = response.reason
    if status in [200, 201]:
        success = True
    else:
        success = False
    d = response.read().decode()
    data = json.loads(d)
    return success, data, status, reason

def http_get(url, headers={}):
    base, path = decode_url(url)
    conn = http.client.HTTPSConnection(base)
    conn.request("GET", url=path, headers=headers)
    response = conn.getresponse()
    status = response.status
    reason = response.reason
    if status in [200, 201]:
        success = True
    else:
        success = False
    d = response.read().decode()
    data = json.loads(d)
    return success, data, status, reason