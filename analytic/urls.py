from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import AnalyticView

# Create a router and register our viewsets with it.
# router = DefaultRouter()
# router.register(r'card', CardList)

# The API URLs are now determined automatically by the router.

# router = DefaultRouter()
# router.register(r'analytic', AnalyticView, '')

urlpatterns = [
    path('', AnalyticView.as_view()),
]