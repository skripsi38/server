from django.shortcuts import render
from rest_framework import views, generics, viewsets
from rest_framework.permissions import IsAuthenticated
from cipher.models import Key, EncryptionLog
from .serializers import AnalyticSerializer
from rest_framework.response import Response

# Create your views here.
class AnalyticView(views.APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        print("hallo")
        key = Key.objects.count()
        encryption = EncryptionLog.objects.count()
        data = {'key': key, 'encryption': encryption}
        return Response(data=data)