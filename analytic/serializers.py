from rest_framework import serializers

class AnalyticSerializer(serializers.Serializer):
    key = serializers.IntegerField()
    encryption = serializers.IntegerField()