from rest_framework import serializers, exceptions
from .models import Card, CardAuthenticationLog
from django.contrib.auth.models import User
from cipher.models import EncryptionLog, Key
from cipher.encryption import ECCEncrypt, NTRUEncrypt
from cipher.decryption import ECCDecrypt, NTRUDecrypt
from datetime import datetime

class UserDetailSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    username = serializers.CharField()

class CardSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    user = UserDetailSerializer(read_only=True)

    class Meta:
        model = Card
        fields = '__all__' 

class CardAuthenticationLogSerialiser(serializers.ModelSerializer):
    class Meta:
        model =  CardAuthenticationLog
        fields = '__all__'

class UserCreateSerializer(serializers.Serializer):
    username = serializers.CharField()
    email = serializers.CharField()
    password = serializers.CharField()

class CardCreateSerializer(CardSerializer):
    def create(self, validated_data):
        request = self.context.get('request')
        ref_number = f"card{int(datetime.now().timestamp())}"
        plain_text = f"{request.user.id}#{request.user.email}#{int(datetime.now().timestamp())}"
        print(validated_data.get('cipher_type'))
        key = Key.objects.filter(sec_level=validated_data.get('sec_level'), cipher_type=validated_data.get('cipher_type')).first()
        if validated_data.get('cipher_type') == "ECC":
            encryption_log = ECCEncrypt(reference_number=ref_number, plain_text=plain_text, sec_level=validated_data.get('sec_level'), source_key=key, destination_key=key)
        elif validated_data.get('cipher_type') == "NTRU":
            encryption_log = NTRUEncrypt(reference_number=ref_number, plain_text=plain_text, sec_level=validated_data.get('sec_level'), source_key=key, destination_key=key)
        user = request.user
        card = Card.objects.create(user=user, encryption_log=encryption_log, reference_number=ref_number, **validated_data)
        return card
    
class CardAuthenticateSerializer(serializers.Serializer):
    serial_number = serializers.CharField()
    public_key = serializers.CharField()
    cipher_text = serializers.CharField()

    def create(self, validated_data):
        serial_number = validated_data.get('serial_number')
        pub_key = validated_data.get('public_key')
        ref_number = f"cardauthentication{int(datetime.now().timestamp())}"

        try:
            card = Card.objects.get(serial_number=serial_number)
        except Card.DoesNotExist:
            raise exceptions.NotFound(f"card with serial number {serial_number} does not exist", code=422)
        
        encryption_log = card.encryption_log
        source_key = encryption_log.key_source
        destination_key = encryption_log.key_destination
        user = card.user

        print(source_key.pub_key)

        if source_key.pub_key != pub_key:
            raise exceptions.ValidationError('invalid card public key', code=422)
        
        print(encryption_log.cipher_type)
        
        if encryption_log.cipher_type == "ECC":
            decrypt_encryption_log = ECCDecrypt(
                reference_number=ref_number, 
                cipher_text=encryption_log.cipher_text, 
                sec_level=encryption_log.sec_level, 
                source_key=destination_key, 
                destination_key=source_key
            )
        elif encryption_log.cipher_type == "NTRU":
            decrypt_encryption_log = NTRUDecrypt(
                reference_number=ref_number, 
                cipher_text=encryption_log.cipher_text, 
                sec_level=encryption_log.sec_level, 
                source_key=destination_key, 
                destination_key=source_key,
            )
        else:
            raise exceptions.NotAcceptable('invalid card ecrypted data')
        
        print(decrypt_encryption_log.plain_text)
        plain_text_decoded = decrypt_encryption_log.plain_text.split("#")
        if plain_text_decoded[0] != user.id and (plain_text_decoded[1] != "" and plain_text_decoded[1] != user.email):
            success = False
        else:
            success = True

        authentication_log = CardAuthenticationLog.objects.create(
            reference_number=ref_number,
            card=card,
            encryption_log=decrypt_encryption_log,
            success=success
        )
        return authentication_log

