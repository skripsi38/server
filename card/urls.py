from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import CardListCreate, CardAuthenticationView, CardDetailView, CardAuthenticationLogListView

# Create a router and register our viewsets with it.
# router = DefaultRouter()
# router.register(r'card', CardList)

# The API URLs are now determined automatically by the router.

router = DefaultRouter()
router.register(r'auth', CardAuthenticationView, basename="auth")
router.register(r'auth/log', CardAuthenticationLogListView, basename="authlog")
router.register(r'', CardListCreate)
router.register(r'', CardDetailView)

urlpatterns = [
    path('', include(router.urls)),
]