# Generated by Django 4.1.4 on 2023-03-07 04:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cipher', '0010_encryptionlog_method_type'),
        ('card', '0003_card_cipher_type_card_sec_level'),
    ]

    operations = [
        migrations.CreateModel(
            name='CardAuthenticationLog',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('success', models.BooleanField()),
                ('card', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='card.card')),
                ('encryption_log', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cipher.encryptionlog')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
