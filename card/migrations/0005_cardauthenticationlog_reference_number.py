# Generated by Django 4.1.4 on 2023-03-07 04:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('card', '0004_cardauthenticationlog'),
    ]

    operations = [
        migrations.AddField(
            model_name='cardauthenticationlog',
            name='reference_number',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
