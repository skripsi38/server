from django.db import models
from core.models import BaseModel
from django.contrib.auth.models import User
from cipher.models import EncryptionLog
from cipher.utils import enum

# Create your models here.
class Card(BaseModel):
    serial_number = models.CharField(unique=True,max_length=32)
    reference_number = models.CharField(max_length=100, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    encryption_log =models.ForeignKey(EncryptionLog, on_delete=models.CASCADE, null=True, blank=True)
    cipher_type = models.CharField(max_length=10, choices=enum.KeyType.choices(), null=True, blank=True)
    sec_level = models.IntegerField(null=True, blank=True)


class CardAuthenticationLog(BaseModel):
    reference_number = models.CharField(max_length=100, null=True, blank=True)
    card = models.ForeignKey(Card, on_delete=models.CASCADE)
    encryption_log = models.ForeignKey(EncryptionLog, on_delete=models.CASCADE)
    success = models.BooleanField()
    