from django_filters import rest_framework as filters, DateTimeFromToRangeFilter
from .models import Card, CardAuthenticationLog

class CardFilter(filters.FilterSet):
    created = DateTimeFromToRangeFilter()

    class Meta:
        model = Card
        fields = ('created', 'serial_number')

class CardAuthenticationLogFilter(filters.FilterSet):
    class Meta:
        model = CardAuthenticationLog
        fields = ('card', 'reference_number', )