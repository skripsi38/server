from django.shortcuts import render
from rest_framework import mixins, viewsets, views, generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.filters import OrderingFilter
from .models import Card, CardAuthenticationLog
from .serializers import CardSerializer, CardCreateSerializer, CardAuthenticateSerializer, CardAuthenticationLogSerialiser
from django_filters import rest_framework as filters
from .filters import CardFilter, CardAuthenticationLogFilter
from rest_framework import status

# Create your views here.
class CardListCreate(generics.ListCreateAPIView, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Card.objects.all()
    filterset_class = CardFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    ordering = ('created')

    def get_serializer_class(self):
        if self.action == 'list':
            return CardSerializer
        elif self.action == 'create':
            return CardCreateSerializer
        
    def create(self, request, *args, **kwargs):
        context = {'request': request}
        serializer = CardCreateSerializer(data=request.data, context=context)
        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        return Response(data=serializer.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
    
class CardAuthenticationView(generics.CreateAPIView, viewsets.GenericViewSet):
    serializer_class = CardAuthenticateSerializer

    def create(self, request, *args, **kwargs):
        serializer = CardAuthenticateSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.save()
            log = CardAuthenticationLogSerialiser(data)
            return Response(data=log.data, status=status.HTTP_200_OK)
        return Response(data=serializer.errors)
            
class CardDetailView(generics.RetrieveAPIView,viewsets.GenericViewSet):
    queryset = Card.objects.all()
    serializer_class = CardSerializer
    permission_classes = [IsAuthenticated]


class CardAuthenticationLogListView(generics.ListAPIView, viewsets.GenericViewSet):
    queryset = CardAuthenticationLog.objects.all()
    serializer_class = CardAuthenticationLogSerialiser
    permission_classes = [IsAuthenticated]
    filterset_class = CardAuthenticationLogFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    ordering = ('created')
