from django.shortcuts import render
from rest_framework import generics, viewsets, views, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from .serializers import UserSerializers, RegisterSerializer
from datetime import datetime

# Create your views here.
class GetAuthenticatedUserView(views.APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        user = request.user
        serializer = UserSerializers(user)
        return Response(data=serializer.data, status=status.HTTP_200_OK)
    
class RegisterUserView(generics.CreateAPIView):
    serializer_class = RegisterSerializer

    def create(self, request, *args, **kwargs):
        user = request.user
        data_to_encrypt = f"{user.id}#{user.email}#{int(datetime.now().timestamp())}"
        print(data_to_encrypt)

        # serializer = RegisterSerializer(data=request.data)
        # if serializer.is_valid():
        #     serializer.create()
        #     return Response(data={'message': 'register success'})
        # return Response(data=serializer.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)