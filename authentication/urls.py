from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import GetAuthenticatedUserView, RegisterUserView

# Create a router and register our viewsets with it.
# router = DefaultRouter()
# router.register(r'card', CardList)

# The API URLs are now determined automatically by the router.

router = DefaultRouter()
# router.register(r'user', GetAuthenticatedUserView, basename='')

urlpatterns = [
    path('user/', GetAuthenticatedUserView.as_view()),
    path('register/', RegisterUserView.as_view())
]