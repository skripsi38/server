from django_filters import rest_framework as filters, DateTimeFromToRangeFilter, OrderingFilter
from .models import Key, EncryptionLog

class KeyFilter(filters.FilterSet):
    created = DateTimeFromToRangeFilter()

    class Meta:
        model = Key
        fields = ('created', 'cipher_type', 'sec_level')

class EncryptionLogFilter(filters.FilterSet):
    class Meta:
        model = EncryptionLog
        fields = ('method_type', 'cipher_type', 'sec_level', 'reference_number',)