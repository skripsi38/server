from enum import Enum

class KeyType(Enum):
    ECC = "ECC"
    NTRU = "NTRU"

    @classmethod
    def choices(cls):
        return [(key.value, key.name) for key in cls]