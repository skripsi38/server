	# Implementation of NTRU Homomorphic encryption
import math
from math import gcd
from .poly import extEuclidPoly, modPoly, multPoly, addPoly, cenPoly, trim, divPoly, isTernary
import base64

class Ntru:
	N=None
	p=None
	q=None
	d=None
	f=None
	g=None
	h=None
	f_p=None
	f_q=None
	D=None
	def __init__(self,N_new,p_new,q_new):
		self.N=N_new
		self.p=p_new
		self.q=q_new
		D=[0]*(self.N+1)
		D[0]=-1		
		D[self.N]=1
		self.D=D
	
	def encode_pub_key(self):
		h = ' '.join([str(i) for i in self.h])
		pub_key = f'{self.N}#{self.p}#{self.q}#[{h}]'
		pub_key_encoded = base64.b64encode(pub_key.encode('utf-8'))
		return pub_key_encoded.decode('utf-8')

	def load_pub_key(self, encoded_pub_key):
		pub_key_decoded = base64.b64decode(encoded_pub_key.encode('utf-8')).decode('utf-8')
		pub_key_lst = pub_key_decoded.split('#')
		self.N = int(pub_key_lst[0])
		self.p = int(pub_key_lst[1])
		self.q = int(pub_key_lst[2])
		D=[0]*(self.N+1)
		D[0]=-1		
		D[self.N]=1
		self.D=D
		h = pub_key_lst[3].replace('[', '')
		h = h.replace(']', '')
		h = h.split(' ')
		h = [int(i) for i in h]
		self.h = h

	def encode_priv_key(self):
		f = ' '.join([str(i) for i in self.f])
		f_p = ' '.join([str(i) for i in self.f_p])
		g = ' '.join([str(i) for i in self.g])
		priv_key = f'[{f}]#[{f_p}]#[{g}]'
		priv_key_encoded = base64.b64encode(priv_key.encode('utf-8'))
		return priv_key_encoded.decode('utf-8')

	def load_priv_key(self, encoded_priv_key):
		priv_key_decoded = base64.b64decode(encoded_priv_key.encode('utf-8'))
		priv_key_decoded_lst = priv_key_decoded.decode('utf-8').split("#")
		priv_key_decoded_lst_int = []
		for i in priv_key_decoded_lst:
			i = i.replace('[', '')
			i = i.replace(']', '')
			temp = i.split(' ')
			temp = [int(i) for i in temp]
			priv_key_decoded_lst_int.append(temp)
		self.f = priv_key_decoded_lst_int[0]
		self.f_p = priv_key_decoded_lst_int[1]
		self.g = priv_key_decoded_lst_int[2]

	def genPublicKey(self,f_new,g_new,d_new):
		# Using Extended Euclidean Algorithm for Polynomials
		# to get s and t. Note that the gcd must be 1
		self.f=f_new
		self.g=g_new
		self.d=d_new
		[gcd_f,s_f,t_f]=extEuclidPoly(self.f,self.D)
		self.f_p=modPoly(s_f,self.p)
		self.f_q=modPoly(s_f,self.q)
		self.h=self.reModulo(multPoly(self.f_q,self.g),self.D,self.q)
		if not self.runTests():
			print("Failed!")
			quit()
			
	def getPublicKey(self):
		return self.h

	def setPublicKey(self,public_key):
		self.h=public_key
	
	def encrypt(self,message,randPol):
		if self.h!=None:
			e_tilda=addPoly(multPoly(multPoly([self.p],randPol),self.h),message)
			e=self.reModulo(e_tilda,self.D,self.q)
			return e
		else:
			print("Cannot Encrypt Message Public Key is not set!")
			print(	"Cannot Set Public Key manually or Generate it")

	def decryptSQ(self,encryptedMessage):
		F_p_sq=multPoly(self.f_p,self.f_p)
		f_sq=multPoly(self.f,self.f)
		tmp=self.reModulo(multPoly(f_sq,encryptedMessage),self.D,self.q)
		centered=cenPoly(tmp,self.q)
		m1=multPoly(F_p_sq,centered)
		tmp=self.reModulo(m1,self.D,self.p)
		return trim(tmp) 

	def decrypt(self,encryptedMessage):
		tmp=self.reModulo(multPoly(self.f,encryptedMessage),self.D,self.q)
		centered=cenPoly(tmp,self.q)
		m1=multPoly(self.f_p,centered)
		tmp=self.reModulo(m1,self.D,self.p)
		return trim(tmp) 

	def reModulo(self,num,div,modby):
		[_,remain]=divPoly(num,div)
		return modPoly(remain,modby)

	def printall(self):
		print(self.N)
		print(self.p)
		print(self.q)
		print(self.f)
		print(self.g)
		print(self.h)
		print(self.f_p)	
		print(self.f_q)
		print(self.D)
	
	def isPrime(self):
		if self.N % 2 == 0 and self.N > 2: 
			return False
		return all(self.N % i for i in range(3, int(math.sqrt(self.N)) + 1, 2))
		
	def runTests(self):			
		# Checking if inputs satisfy conditions
		if not self.isPrime() :
			print("Error: N is not prime!")
			return False
	
		if gcd(self.N,self.p) != 1 :
			print("Error: gcd(N,p) is not 1")
			return False

		if gcd(self.N,self.q)!=1 :
			print("Error: gcd(N,q) is not 1")
			return False
	
		if self.q<=(6*self.d+1)*self.p:
			print("Error: q is not > (6*d+1)*p")
			return False
	
		if not isTernary(self.f,self.d+1,self.d):
			print("Error: f does not belong to T(d+1,d)")
			return False
		
		if not isTernary(self.g,self.d,self.d):
			print("Error: g does not belong to T(d,d)")
			return False
	
		return True		
	

