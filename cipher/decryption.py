from .utils import ecc
from .utils.ntru.ntru import Ntru
import base64
import time
import json
from core.utils.http_request import http_post
from .utils.enum import KeyType
from .models import EncryptionLog, MethodType
from .utils.ntru.utils import str_to_bin, encode64, decode64, bin_to_str

def ECCDecrypt(reference_number, cipher_text, sec_level, source_key, destination_key):
    ec_source = ecc.Ecc(security_level=source_key.sec_level)
    ec_destination = ecc.Ecc(security_level=destination_key.sec_level)
    priv_key_source = int(base64.b64decode(source_key.priv_key.encode('utf-8')).decode('utf-8'))
    priv_key_destination = int(base64.b64decode(destination_key.priv_key.encode('utf-8')).decode('utf-8'))
    pub_key_destination = ec_destination.calculate_pub_key(priv_key=priv_key_destination)
    msg = cipher_text.encode('utf-8')
    decoded_msg = base64.b64decode(msg)
    decoded_msg = decoded_msg.split()
    print(decoded_msg)
    ciphertext = base64.b64decode(decoded_msg[0])
    nonce = base64.b64decode(decoded_msg[1])
    tag = base64.b64decode(decoded_msg[2])
    t1 = time.perf_counter_ns()
    shared_key = priv_key_source * pub_key_destination
    shared_key_byte = ec_destination.ecc_point_to_256_bit_key(shared_key)
    plain_text = ec_source.decrypt_aed(ciphertext=ciphertext, nonce=nonce, tag=tag, secret=shared_key_byte)
    print(plain_text)
    t2 = time.perf_counter_ns()
    # data = self.validated_data
    # d = {x: data[x] for x in data if x not in ['source_priv_key', 'destination_pub_key']}
    # body = json.dumps({
    #     "app_code": "skripsi",
    #     "body": "New decription ECC log",
    #     "additional_data": {
    #         "reference_number": reference_number
    #     }
    # })
    # http_post('https://pusher.kuadran.co/message', {}, body)
    return EncryptionLog.objects.create(
        reference_number=reference_number,
        cipher_type=KeyType.ECC.value,
        key_source=source_key, 
        key_destination=destination_key, 
        cipher_input_text=cipher_text,
        plain_text=plain_text.decode('utf-8'), 
        sec_level=sec_level,
        decryption_time=t2-t1, method_type=MethodType.DECRYPT
    )

def NTRUDecrypt(reference_number, cipher_text, sec_level, source_key, destination_key):
    rand_pol=[-1,-1,1,1]
    msg = decode64(cipher_text.encode('utf-8'))
    encoded_priv_key = source_key.priv_key
    encoded_pub_key = destination_key.pub_key
    cipher = Ntru(251, 3, 2048)
    cipher.load_priv_key(encoded_priv_key)
    cipher.load_pub_key(encoded_pub_key)
    t1 = time.perf_counter_ns()
    plain_text = cipher.decrypt(msg)
    t2 = time.perf_counter_ns()
    plain_text_str = bin_to_str(plain_text)
    # d = {x: data[x] for x in data if x not in ['source_priv_key', 'destination_pub_key']}
    # body = json.dumps({
    #     "app_code": "skripsi",
    #     "body": "New decription NTRU log",
    #     "additional_data": {
    #         "reference_number": reference_number
    #     }
    # })
    # http_post('https://pusher.kuadran.co/message', {}, body)
    return EncryptionLog.objects.create(
        reference_number=reference_number,
        cipher_type=KeyType.NTRU.value,
        key_source=source_key, 
        key_destination=destination_key, 
        cipher_input_text=cipher_text,
        plain_text=plain_text_str, 
        sec_level=sec_level,
        decryption_time=t2-t1, method_type=MethodType.DECRYPT
    )
