from rest_framework import serializers, status
import base64, time
from .models import Key, EncryptionLog, MethodType
from .utils import ecc, enum
from .utils.ntru.ntru import Ntru
from .utils.ntru.utils import str_to_bin, encode64, decode64, bin_to_str
from core.utils.http_request import http_post
import json

class KeySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    priv_key = serializers.CharField(read_only=True)
    pub_key = serializers.CharField(read_only=True)
    cipher_type = serializers.CharField(read_only=True)

    class Meta:
        model = Key
        fields = "__all__"

class CreateKeySerializer(KeySerializer):
    cipher_type = serializers.ChoiceField(choices=enum.KeyType.choices())

    def create(self, validated_data):
        sec_level = validated_data.get('sec_level')
        cipher_type = validated_data.get('cipher_type')
        if cipher_type == enum.KeyType.ECC.value:
            curve = ecc.Ecc(security_level=sec_level)
            t1 = time.perf_counter_ns()
            priv_key = curve.generate_priv_key()
            pub_key = curve.calculate_pub_key(priv_key=priv_key)
            t2 = time.perf_counter_ns()
            priv_key_str = base64.b64encode(str(priv_key).encode('utf-8')).decode('utf-8')
            pub_key_str = curve.get_pub_key_str(pubKey=pub_key)
            return Key.objects.create(pub_key=pub_key_str, priv_key=priv_key_str, cipher_type=enum.KeyType.ECC.value, sec_level=sec_level, generation_time=t2-t1)
        elif cipher_type == enum.KeyType.NTRU.value:
            print("oke")
            p = 3
            q = 2048
            if sec_level == 80:
                N = 251
            elif sec_level == 112:
                N = 401
            elif sec_level == 128:
                N = 443
            elif sec_level == 160:
                N = 491
            elif sec_level == 192:
                N = 593
            elif sec_level == 256:
                N = 743
            else:
                raise serializers.ValidationError({'sec_level': 'Please enter valid security level'})
            t1 = time.perf_counter_ns()
            ntru = Ntru(N_new=N, p_new=p, q_new=q)
            f=[1,0,1,-1,0,-1,1]
            # f=[1,1,-1,0,-1,1]
            g=[-1,0,1,1,0,0,-1]
            d=2
            ntru.genPublicKey(f_new=f, g_new=g, d_new=d)
            t2 = time.perf_counter_ns()
            pub_key = ntru.encode_pub_key()
            priv_key = ntru.encode_priv_key()
            return Key.objects.create(pub_key=pub_key, priv_key=priv_key, cipher_type=enum.KeyType.NTRU.value, sec_level=sec_level, generation_time=t2-t1)

class EncryptionLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = EncryptionLog
        fields = "__all__"

class DecryptionSerializer(serializers.Serializer):
    reference_number = serializers.CharField(required=False)
    cipher_input_text = serializers.CharField()
    source_priv_key = serializers.CharField()
    destination_pub_key = serializers.CharField()
    cipher_type = serializers.ChoiceField(choices=enum.KeyType.choices())
    sec_level = serializers.IntegerField()

    def save(self):
        cipher_type = self.validated_data.get('cipher_type')
        encoded_priv_key = self.validated_data.get('source_priv_key')
        encoded_pub_key = self.validated_data.get('destination_pub_key')
        source_key = Key.objects.filter(cipher_type=cipher_type, priv_key=encoded_priv_key).first()
        destination_key = Key.objects.filter(cipher_type=cipher_type, pub_key=encoded_pub_key).first()

        if (source_key == None):
            raise serializers.ValidationError(detail='source key not found', code=status.HTTP_422_UNPROCESSABLE_ENTITY)

        cipher_text = self.validated_data.get('cipher_input_text')

        if cipher_type == enum.KeyType.ECC.value:
            ec_source = ecc.Ecc(security_level=source_key.sec_level)
            ec_destination = ecc.Ecc(security_level=destination_key.sec_level)
            priv_key_source = int(base64.b64decode(encoded_priv_key.encode('utf-8')).decode('utf-8'))
            priv_key_destination = int(base64.b64decode(destination_key.priv_key.encode('utf-8')).decode('utf-8'))
            pub_key_destination = ec_destination.calculate_pub_key(priv_key=priv_key_destination)
            msg = cipher_text.encode('utf-8')
            decoded_msg = base64.b64decode(msg)
            decoded_msg = decoded_msg.split()
            print(decoded_msg)
            ciphertext = base64.b64decode(decoded_msg[0])
            nonce = base64.b64decode(decoded_msg[1])
            tag = base64.b64decode(decoded_msg[2])
            t1 = time.perf_counter_ns()
            shared_key = priv_key_source * pub_key_destination
            shared_key_byte = ec_destination.ecc_point_to_256_bit_key(shared_key)
            plain_text = ec_source.decrypt_aed(ciphertext=ciphertext, nonce=nonce, tag=tag, secret=shared_key_byte)
            print(plain_text)
            t2 = time.perf_counter_ns()
            data = self.validated_data
            d = {x: data[x] for x in data if x not in ['source_priv_key', 'destination_pub_key']}
            # body = json.dumps({
            #     "app_code": "skripsi",
            #     "body": "New decription ECC log",
            #     "additional_data": {
            #         "reference_number": self.validated_data.get('reference_number')
            #     }
            # })
            # http_post('https://pusher.kuadran.co/message', {}, body)
            return EncryptionLog.objects.create(**d, key_source=source_key, key_destination=destination_key, plain_text=plain_text.decode('utf-8'), decryption_time=t2-t1, method_type=MethodType.DECRYPT)
    
        elif cipher_type == enum.KeyType.NTRU.value:
            rand_pol=[-1,-1,1,1]
            msg = decode64(cipher_text.encode('utf-8'))
            cipher = Ntru(251, 3, 2048)
            cipher.load_priv_key(encoded_priv_key)
            cipher.load_pub_key(encoded_pub_key)
            t1 = time.perf_counter_ns()
            plain_text = cipher.decrypt(msg)
            t2 = time.perf_counter_ns()
            plain_text_str = bin_to_str(plain_text)
            data = self.validated_data
            d = {x: data[x] for x in data if x not in ['source_priv_key', 'destination_pub_key']}
            # body = json.dumps({
            #     "app_code": "skripsi",
            #     "body": "New decription NTRU log",
            #     "additional_data": {
            #         "reference_number": self.validated_data.get('reference_number')
            #     }
            # })
            # http_post('https://pusher.kuadran.co/message', {}, body)
            return EncryptionLog.objects.create(**d, key_source=source_key, key_destination=destination_key, plain_text=plain_text_str, decryption_time=t2-t1, method_type=MethodType.DECRYPT)            


class EncryptionSerializer(serializers.Serializer):
    reference_number = serializers.CharField(required=False)
    plain_input_text = serializers.CharField()
    source_priv_key = serializers.CharField()
    destination_pub_key = serializers.CharField()
    cipher_type = serializers.ChoiceField(choices=enum.KeyType.choices())
    sec_level = serializers.IntegerField()

    def save(self):
        cipher_type = self.validated_data.get('cipher_type')
        encoded_priv_key = self.validated_data.get('source_priv_key')
        encoded_pub_key = self.validated_data.get('destination_pub_key')
        source_key = Key.objects.filter(cipher_type=cipher_type, priv_key=encoded_priv_key).first()
        destination_key = Key.objects.filter(cipher_type=cipher_type, pub_key=encoded_pub_key).first()

        if (source_key == None):
            raise serializers.ValidationError(detail='source priv key not found', code=status.HTTP_422_UNPROCESSABLE_ENTITY)

        plain_text = self.validated_data.get('plain_input_text')
        print("DEbug:", plain_text)
        if cipher_type == enum.KeyType.ECC.value:
            ec_source = ecc.Ecc(security_level=source_key.sec_level)
            ec_destination = ecc.Ecc(security_level=destination_key.sec_level)
            priv_key_source = int(base64.b64decode(encoded_priv_key.encode('utf-8')).decode('utf-8'))
            priv_key_destination = int(base64.b64decode(destination_key.priv_key.encode('utf-8')).decode('utf-8'))
            pub_key_destination = ec_destination.calculate_pub_key(priv_key=priv_key_destination)
            t1 = time.perf_counter_ns()
            shared_key = priv_key_source * pub_key_destination
            shared_key_byte = ec_destination.ecc_point_to_256_bit_key(shared_key)
            msg = plain_text.encode('utf-8')
            ciphertext, nonce, header, tag = ec_source.encrypt_aed(msg, shared_key_byte)
            t2 = time.perf_counter_ns()
            ciphertextb64 = base64.b64encode(ciphertext)
            nonceb64 = base64.b64encode(nonce)
            tagb64 = base64.b64encode(tag)
            encrypted_msg = ciphertextb64 + b" " + nonceb64 + b" " + tagb64
            print(encrypted_msg)
            encrypted_msg_b64 = base64.b64encode(encrypted_msg)
            data = self.validated_data
            d = {x: data[x] for x in data if x not in ['source_priv_key', 'destination_pub_key']}
            # body = json.dumps({
            #     "app_code": "skripsi",
            #     "body": "New encription ECC log",
            #     "additional_data": {
            #         "reference_number": self.validated_data.get('reference_number')
            #     }
            # })
            # http_post('https://pusher.kuadran.co/message', {}, body)
            return EncryptionLog.objects.create(**d, key_source=source_key, key_destination=destination_key, cipher_text=encrypted_msg_b64.decode('utf-8'), encryption_time=t2-t1, method_type=MethodType.ENCRYPT)

        elif cipher_type == enum.KeyType.NTRU.value:
            rand_pol=[-1,-1,1,1]
            msg = str_to_bin(plain_text)
            cipher = Ntru(251, 3, 2048)
            cipher.load_priv_key(encoded_priv_key)
            cipher.load_pub_key(encoded_pub_key)
            t1 = time.perf_counter_ns()
            cipher_text = cipher.encrypt(msg, rand_pol)
            t2 = time.perf_counter_ns()
            cipher_text_encoded = encode64(cipher_text)
            data = self.validated_data
            d = {x: data[x] for x in data if x not in ['source_priv_key', 'destination_pub_key']}
            # body = json.dumps({
            #     "app_code": "skripsi",
            #     "body": "New encription NTRU log",
            #     "additional_data": {
            #         "reference_number": self.validated_data.get('reference_number')
            #     }
            # })
            # http_post('https://pusher.kuadran.co/message', {}, body)
            return EncryptionLog.objects.create(**d, key_source=source_key, key_destination=destination_key, cipher_text=cipher_text_encoded.decode('utf-8'), encryption_time=t2-t1, method_type=MethodType.ENCRYPT)
