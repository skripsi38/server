from .utils import ecc
from .utils.ntru.ntru import Ntru
from .utils.enum import KeyType
import time
import base64
import json
from core.utils.http_request import http_post
from .utils.ntru.utils import str_to_bin, encode64, decode64, bin_to_str
from .models import EncryptionLog, MethodType

def ECCEncrypt(reference_number, plain_text, sec_level, source_key, destination_key):
    ec_source = ecc.Ecc(security_level=source_key.sec_level)
    ec_destination = ecc.Ecc(security_level=destination_key.sec_level)
    priv_key_source = int(base64.b64decode(source_key.priv_key.encode('utf-8')).decode('utf-8'))
    priv_key_destination = int(base64.b64decode(destination_key.priv_key.encode('utf-8')).decode('utf-8'))
    pub_key_destination = ec_destination.calculate_pub_key(priv_key=priv_key_destination)
    t1 = time.perf_counter_ns()
    shared_key = priv_key_source * pub_key_destination
    shared_key_byte = ec_destination.ecc_point_to_256_bit_key(shared_key)
    msg = plain_text.encode('utf-8')
    ciphertext, nonce, header, tag = ec_source.encrypt_aed(msg, shared_key_byte)
    t2 = time.perf_counter_ns()
    ciphertextb64 = base64.b64encode(ciphertext)
    nonceb64 = base64.b64encode(nonce)
    tagb64 = base64.b64encode(tag)
    encrypted_msg = ciphertextb64 + b" " + nonceb64 + b" " + tagb64
    print(encrypted_msg)
    encrypted_msg_b64 = base64.b64encode(encrypted_msg)
    # data = self.validated_data
    # d = {x: data[x] for x in data if x not in ['source_priv_key', 'destination_pub_key']}
    # body = json.dumps({
    #     "app_code": "skripsi",
    #     "body": "New encription ECC log",
    #     "additional_data": {
    #         "reference_number": reference_number
    #     }
    # })
    # http_post('https://pusher.kuadran.co/message', {}, body)
    return EncryptionLog.objects.create(
        reference_number=reference_number,
        cipher_type=KeyType.ECC.value,
        key_source=source_key, 
        key_destination=destination_key, 
        plain_input_text=plain_text,
        cipher_text=encrypted_msg_b64.decode('utf-8'), 
        encryption_time=t2-t1,
        sec_level=sec_level,
        method_type=MethodType.ENCRYPT
    )
    # return EncryptionLog.objects.create(**d, key_source=source_key, key_destination=destination_key, cipher_text=encrypted_msg_b64.decode('utf-8'), encryption_time=t2-t1, method_type=MethodType.ENCRYPT)

def NTRUEncrypt(reference_number, plain_text, sec_level, source_key, destination_key):
    rand_pol=[-1,-1,1,1]
    msg = str_to_bin(plain_text)
    cipher = Ntru(251, 3, 2048)
    encoded_priv_key = source_key.priv_key
    encoded_pub_key = destination_key.pub_key
    cipher.load_priv_key(encoded_priv_key)
    cipher.load_pub_key(encoded_pub_key)
    t1 = time.perf_counter_ns()
    cipher_text = cipher.encrypt(msg, rand_pol)
    t2 = time.perf_counter_ns()
    cipher_text_encoded = encode64(cipher_text)
    # data = self.validated_data
    # d = {x: data[x] for x in data if x not in ['source_priv_key', 'destination_pub_key']}
    # body = json.dumps({
    #     "app_code": "skripsi",
    #     "body": "New encription NTRU log",
    #     "additional_data": {
    #         "reference_number": reference_number
    #     }
    # })
    # http_post('https://pusher.kuadran.co/message', {}, body)
    return EncryptionLog.objects.create(
        reference_number=reference_number,
        cipher_type=KeyType.NTRU.value,
        key_source=source_key, 
        key_destination=destination_key, 
        plain_input_text=plain_text,
        cipher_text=cipher_text_encoded.decode('utf-8'), 
        encryption_time=t2-t1, 
        sec_level=sec_level,
        method_type=MethodType.ENCRYPT
    )
    # return EncryptionLog.objects.create(**d, key_source=source_key, key_destination=destination_key, cipher_text=cipher_text_encoded.decode('utf-8'), encryption_time=t2-t1, method_type=MethodType.ENCRYPT)