from django.shortcuts import render
from rest_framework import generics, viewsets, status
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from .models import Key, EncryptionLog
from .serializers import CreateKeySerializer, KeySerializer, EncryptionSerializer, EncryptionLogSerializer, DecryptionSerializer
from django_filters import rest_framework as filters
from .filters import KeyFilter, EncryptionLogFilter
from rest_framework.permissions import IsAuthenticated

# Create your views here.
class ListCreateKey(generics.ListCreateAPIView, viewsets.GenericViewSet):
    queryset = Key.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = KeyFilter
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        if self.action == 'list':
            return KeySerializer
        elif self.action == 'create':
            return CreateKeySerializer

class DetailKey(generics.RetrieveDestroyAPIView, viewsets.GenericViewSet):
    queryset = Key.objects.all()
    serializer_class = KeySerializer
    permission_classes = [IsAuthenticated]

    # def destroy(request, format=None):
    #     # data = Key.objects.get(request.data.id)
    #     # data.delete()
    #     return Response('Key has been deleted', status=status.HTTP_204_NO_CONTENT)

class EncryptView(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]
    # queryset = EncryptionLog.objects.all()
    def create(self, request, format=None):
        serializer = EncryptionSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.save()
            serializer_data = EncryptionLogSerializer(data)
            return Response(serializer_data.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DecryptView(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]
    # queryset = EncryptionLog.objects.all()
    def create(self, request, format=None):
        serializer = DecryptionSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.save()
            serializer_data = EncryptionLogSerializer(data)
            return Response(serializer_data.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class EncryptionLogList(generics.ListAPIView, viewsets.GenericViewSet):
    queryset = EncryptionLog.objects.all()
    serializer_class = EncryptionLogSerializer
    permission_classes = [IsAuthenticated]
    filterset_class = EncryptionLogFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    ordering = ('created')

class EncryptionLogDetail(generics.RetrieveAPIView, generics.DestroyAPIView, viewsets.GenericViewSet):
    queryset = EncryptionLog.objects.all()
    serializer_class = EncryptionLogSerializer
    permission_classes = [IsAuthenticated]