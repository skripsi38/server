from django.db import models
from core.models import BaseModel
from .utils import enum

# Create your models here.
class Key(BaseModel):
    pub_key = models.TextField()
    priv_key = models.TextField()
    cipher_type = models.CharField(max_length=10, choices=enum.KeyType.choices(), default=enum.KeyType.ECC)
    sec_level = models.IntegerField(default=80)
    generation_time = models.BigIntegerField(default=0)

class MethodType(models.TextChoices):
    ENCRYPT = 'encryption'
    DECRYPT = 'decryption'

class EncryptionLog(BaseModel):
    reference_number = models.CharField(max_length=50, blank=True, null=True)
    key_source = models.ForeignKey(Key, on_delete=models.CASCADE, related_name='encryption_key_source')
    key_destination = models.ForeignKey(Key, on_delete=models.CASCADE, related_name='encryption_key_destination')
    cipher_type = models.CharField(max_length=10, choices=enum.KeyType.choices())
    sec_level = models.IntegerField()
    plain_input_text = models.TextField(blank=True, null=True)
    plain_text = models.TextField(blank=True, null=True)
    cipher_input_text = models.TextField(blank=True, null=True)
    cipher_text = models.TextField(blank=True, null=True)
    encryption_time = models.IntegerField(null=True, blank=True)
    decryption_time = models.IntegerField(null=True, blank=True)
    method_type = models.CharField(max_length=15, choices=MethodType.choices, null=True, blank=True)

