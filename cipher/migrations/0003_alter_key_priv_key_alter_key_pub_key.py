# Generated by Django 4.1.5 on 2023-01-08 10:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("cipher", "0002_key_generation_time"),
    ]

    operations = [
        migrations.AlterField(
            model_name="key", name="priv_key", field=models.CharField(max_length=512),
        ),
        migrations.AlterField(
            model_name="key", name="pub_key", field=models.CharField(max_length=512),
        ),
    ]
