# Generated by Django 4.1.4 on 2023-01-17 08:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cipher', '0008_alter_encryptionlog_decryption_time_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='key',
            name='generation_time',
            field=models.BigIntegerField(default=0),
        ),
    ]
