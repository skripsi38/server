from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ListCreateKey, DetailKey, EncryptView, DecryptView, EncryptionLogList, EncryptionLogDetail

# Create a router and register our viewsets with it.
# router = DefaultRouter()
# router.register(r'card', CardList)

# The API URLs are now determined automatically by the router.

router = DefaultRouter()
router.register(r'key', ListCreateKey)
router.register(r'key', DetailKey)
router.register(r'encrypt', EncryptView, 'encryption')
router.register(r'decrypt', DecryptView, 'decryption')
router.register(r'encryption', EncryptionLogList)
router.register(r'encryption', EncryptionLogDetail)

urlpatterns = [
    path('', include(router.urls)),
]